// Copyright Epic Games, Inc. All Rights Reserved.


#include "SpaceShipGameModeBase.h"
#include "GameplayPlayerController.h"
#include "SpaceShipPawn.h"

ASpaceShipGameModeBase::ASpaceShipGameModeBase()
{
	PlayerControllerClass = AGameplayPlayerController::StaticClass();
	DefaultPawnClass = ASpaceShipPawn::StaticClass();
}