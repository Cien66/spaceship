// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SpaceShipPawn.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class SPACESHIP_API ASpaceShipPawn : public APawn
{
	GENERATED_BODY()

public:
	ASpaceShipPawn();

	UPROPERTY(VisibleAnywhere, Category = "SpaceShipPawn")
	UStaticMeshComponent* StaticMeshComponent{nullptr};

	UPROPERTY(VisibleAnywhere, Category = "SpaceShipPawn")
	USpringArmComponent* SpringArmComponent = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "SpaceShipPawn")
	UCameraComponent* CameraComponent = nullptr;

	UPROPERTY(EditAnywhere, Category = "SpaceShipPawn")
	float Speed = 500.f;

	void MoveForward(float Value);
	void MoveRight(float Value);

	void StartShot();
	void StopShot();
};
