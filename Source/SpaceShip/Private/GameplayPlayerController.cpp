// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayPlayerController.h"
#include "SpaceShipPawn.h"

AGameplayPlayerController::AGameplayPlayerController()
{
	AutoReceiveInput = EAutoReceiveInput::Player0;
}

void AGameplayPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Shot", EInputEvent::IE_Pressed, this, &AGameplayPlayerController::OnStartShot);
	InputComponent->BindAction("Shot", EInputEvent::IE_Released, this, &AGameplayPlayerController::OnStopShot);

	InputComponent->BindAxis("MoveForward", this, &AGameplayPlayerController::OnMoveForward);
	InputComponent->BindAxis("MoveRight", this, &AGameplayPlayerController::OnMoveRight);
}

void AGameplayPlayerController::OnStartShot()
{
	GEngine->AddOnScreenDebugMessage(-1, .5f, FColor::Red, "Start shot!", true, FVector2D(5, 5));
}

void AGameplayPlayerController::OnStopShot()
{
	GEngine->AddOnScreenDebugMessage(-1, .5f, FColor::Blue, "Stop shot!", true, FVector2D(5, 10));
}

void AGameplayPlayerController::OnMoveForward(float Value)
{
	if (ASpaceShipPawn* SpaceShipPawn = Cast<ASpaceShipPawn>(GetPawn()))
	{
		SpaceShipPawn->MoveForward(Value);
	}
}

void AGameplayPlayerController::OnMoveRight(float Value)
{
	if (ASpaceShipPawn* SpaceShipPawn = Cast<ASpaceShipPawn>(GetPawn()))
	{
		SpaceShipPawn->MoveRight(Value);
	}
}
