// Fill out your copyright notice in the Description page of Project Settings.


#include "SpaceShipPawn.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

ASpaceShipPawn::ASpaceShipPawn()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetupAttachment(RootComponent);
	StaticMeshComponent->SetWorldRotation(FRotator(-90.f, 0.f, 0.f));

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->SetWorldRotation(FRotator(-90.f, 0.f, 0.f));
	SpringArmComponent->bEnableCameraLag = true;
	SpringArmComponent->CameraLagSpeed = 1.f;
	SpringArmComponent->TargetArmLength = 600.f;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshFinder(TEXT("/Game/StarterContent/Shapes/Shape_Cone.Shape_Cone"));
	UStaticMesh* StaticMesh = StaticMeshFinder.Object;

	if (StaticMesh != nullptr)
	{
		StaticMeshComponent->SetStaticMesh(StaticMesh);
	}
}

void ASpaceShipPawn::MoveForward(float Value)
{
	FVector Forward = GetActorForwardVector();
	FVector CurrentLocation = GetActorLocation();
	
	float DeltaSeconds = GetWorld() ? GetWorld()->GetDeltaSeconds() : 1.f;

	FVector NewLocation = CurrentLocation + Forward * DeltaSeconds * Speed * Value;

	SetActorLocation(NewLocation);
}

void ASpaceShipPawn::MoveRight(float Value)
{
	FVector Right = GetActorRightVector();
	FVector CurrentLocation = GetActorLocation();

	float DeltaSeconds = GetWorld() ? GetWorld()->GetDeltaSeconds() : 1.f;

	FVector NewLocation = CurrentLocation + Right * DeltaSeconds * Speed * Value;

	SetActorLocation(NewLocation);
}

void ASpaceShipPawn::StartShot()
{
}

void ASpaceShipPawn::StopShot()
{
}
