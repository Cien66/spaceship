// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameplayPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SPACESHIP_API AGameplayPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
		AGameplayPlayerController();

protected:
	virtual void SetupInputComponent() override;

private:
	UFUNCTION()
	void OnStartShot();
	UFUNCTION()
	void OnStopShot();

	UFUNCTION()
	void OnMoveForward(float Value);
	UFUNCTION()
	void OnMoveRight(float Value);
};
